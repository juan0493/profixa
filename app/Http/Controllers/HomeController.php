<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
  public function index() {
    return view('home');
  }

  public function aboutUs() {
    return view('about');
  }

  public function insights() {
    return view('insights');
  }

  public function services() {
    return view('services');
  }

  public function contact() {
    return view('contact');
  }
  
  public function pricing() {
    return view('pricing');
  }

  public function requestConsultation() {
    return view('requestConsultation');
  }

  public function download($file) {
    if ($file === 'application') {
      $fileName = 'Oregon employment application sample.docx';
      $headers = ['Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'];
    } else if($file === 'eligibility') {
      $fileName = 'i-9_Eligibility.pdf';
      $headers = ['Content-Type: application/pdf'];
    } else {
      $fileName = 'W4-Federal.pdf';
      $headers = ['Content-Type: application/pdf'];
    }    
    $myFile = public_path($fileName);
    return response()->download($myFile, $fileName, $headers);
  }

  public function language(Request $request, $lan) {
    $nextLanguage = strtolower($lan);

    if ($nextLanguage !== 'es' && $nextLanguage !== 'en') {
      $nextLanguage = 'en';
    }

    session(['lang' => $nextLanguage]);

    return back();
  }

}
