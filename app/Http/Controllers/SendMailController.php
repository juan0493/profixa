<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\HomeContactMail;
use App\Mail\RequestContactMail;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
  public function home(Request $request) {
    $data = [
      'email' => $request->email,
      'msg' => $request->msg,
      'name' => $request->name,
      'subject' => $request->subject,
    ];
    
    Mail::to(env('MAIL_TO', 'info@profixa365.com'))->send(new HomeContactMail($data));
    return 'success';
  }

  public function request(Request $request) {
    $data = [
      'name' => $request->name,
      'email' => $request->email,
      'pnumber' => $request->pnumber,
      'company_name' => $request->company_name,
      'company_size' => $request->company_size,
      'msg' => $request->msg,
    ];
    
    Mail::to(env('MAIL_TO', 'info@profixa365.com'))->send(new RequestContactMail($data));
    return 'success';
  }
}
