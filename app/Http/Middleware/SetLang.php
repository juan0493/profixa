<?php

namespace App\Http\Middleware;

use Closure;

class SetLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('lang')) {
            $request->session()->put('lang', 'en');
        }
        
        \App::setLocale(session('lang', 'en'));

        return $next($request);
    }
}
