import axios from 'axios';
import Swal from 'sweetalert2';
import validator from 'email-validator';

const capitalize = (s) => {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1).toLowerCase();
}

const errorMessage = () => {
  Swal.fire({
    title: emailMessages.errorTitle,
    text: emailMessages.errorMessage,
    type: 'warning',
    confirmButtonText: emailMessages.close,
    confirmButtonColor: '#666',
  });
};

const send = (url, data) => {
  Swal.fire({
    title: emailMessages.sending,
    html: '',
    onOpen: () => {
      Swal.showLoading();
    },
  });

  axios
    .post(url, data)
    .then((response) => {
      Swal.close();
      if (response.data === 'success') {
        Swal.fire({
          title: emailMessages.successTitle,
          text: emailMessages.successMessage,
          type: 'success',
          confirmButtonText: emailMessages.close,
          confirmButtonColor: '#666',
        });
      } else {
        errorMessage();
      }
    })
    .catch(() => {
      Swal.close();
      errorMessage();
    });
};

const getValue = (id) => document.getElementById(id).value;

const SendMessage = (ev) => {
  ev.preventDefault();
  const data = {
    name: getValue('home-name'),
    email: getValue('home-email'),
    subject: getValue('home-subject'),
    msg: getValue('home-msg'),
  };

  if (validator.validate(data.email)) {
    send('/contact-message', data);
  } else {
    Swal.fire(capitalize(emailMessages.errEmail), '', 'error');
  }
};

const SendMessageRequest = (ev) => {
  ev.preventDefault();  
  const data = {
    name: getValue('request-name'),
    email: getValue('request-email'),
    pnumber: getValue('request-pnumber'),
    company_name: getValue('request-company_name'),
    company_size: getValue('request-company_size'),
    msg: getValue('request-msg'),    
  };

  if (validator.validate(data.email)) {
    send('/request-message', data);
  } else {
    Swal.fire(capitalize(emailMessages.errEmail), '', 'error');
  }
};

document.addEventListener('DOMContentLoaded', function () {
  const elems = document.querySelectorAll('.sidenav');
  M.Sidenav.init(elems);
  
  const homeMail = document.getElementById('sendMail');
  if (homeMail) {
    homeMail.addEventListener('submit', SendMessage);
  }

  const requestMail = document.getElementById('sendMailRequest');
  if (requestMail) {
    requestMail.addEventListener('submit', SendMessageRequest);
  }
});