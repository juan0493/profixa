<?php

return [
  'titleText' => 'OUR FIRM',
  'text' => 'Profixa365 LLC is a professional  firm providing clients with accounting and business solutions. Our proactive and hands-on experienced
            professionals offer growth driven solutions to small, medium, and large businesses as well as individuals.
            <br><br>
            Profixa365 prides itself in offering a bureaucratic free environment without red tape and direct contact with principal partners.
            Our clients look for us, because we are here when they need us, no waits, no excuses.
            <br><br>
            We offer personal attention, 365 days a year. Our clients have the satisfaction of knowing that if something unexpected comes up,
            they can count on us. We provide a unique depth of knowledge, proactiveness and experience during each business engagement.',
  'missionTitle' => 'OUR MISSION',
  'mission' => 'To be there when you need us.<br>Our mission is to improve our client\'s profitability by keeping open communication and provide proactive solutions right when it\'s needed.',
  'visionTitle' => 'OUR VISION',
  'vision' => 'To provide unique, bureaucratic free,  professional accounting and business solutions tailored for each client.',
  'me' => 'As a business woman, I\'m passionate about helping my clients to implement systems that provide  them  a path to run 
          their business with minimal effort and obtain well deserved returns. As an important part of my family and community, 
          I am responsible for teaching my daughter that learning is a life long endeavor and that  such learning must be applied 
          for the well being of both, our family and community.',
  'bio1' => 'Elizabeth has extensive cumulative experience in the areas of management, accounting, team development training and coaching.  
            Throughout her career, and beginning at an early age while helping in the family business, she learned the crucial importance 
            of financial management.  Elizabeth has worked for large and small organizations supporting their accounting and HR efforts in 
            various formats. She has a long track record in the Salem area working for Monrovia for nearly 10 years in planning and budgeting 
            operations, as well as training and development of personnel on internal computer system practices.  Additionally, she has accumulated 
            extensive experience in the construction industry, with in depth understanding of the industry’s business cycle from contracting, project 
            management, material purchasing, billing and forecasting, labor management, as well as the management of the HR functions.',
  'bio2' => 'Elizabeth is most at home developing and deploying strategic system approaches to help her clients achieve their financial goals. 
            At the forefront of these efforts, is making sure systems provide value added and make it easier for the company to obtain high 
            return on investment.  She believes that Return On Investement can be measured and analyzed in all aspects of the business cycle.  
            Currently, Elizabeth ensures that accounts payable, receivable, payroll, billing, and HR procedures are kept up to date and implemented 
            effectively throughout for various businesses.  This also includes following wage and hour laws, workers compensation requirements , 
            Tax compliance , IRS and State filings.
            <br>
            This cumulative experience in the finance and accounting fields has given Elizabeth, the opportunity to become a flexible team player 
            able to bridge the gap in many different situations in the corporate and business environments.  Elizabeth, is fully committed to 
            your company’s success by providing a proactive unique set of solutions for your business.',
];
