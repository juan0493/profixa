<?php

return [
  'title' => 'Pricing',
  't1' => 'Each of our clients is unique. As such, every solution is strategically 
          designed to fit your business needs and help you to increase profits.',
  't2' => 'Our fees are the one thing that you do not have to worry about. They are 
          typically 50% less than most  providers nationwide.',
  'link' => 'Contact us now to get your quote',
];
