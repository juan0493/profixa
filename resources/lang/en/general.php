<?php
return [
  'about' => 'About us',
  'services' => 'Services',
  'contact' => 'Contact',
  'pricing' => 'Pricing',
  'rmore' => 'read more',
  'insight' => 'Insights',
  'menu' => [
    'hom' => 'HOME',
    'abo' => 'ABOUT US',
    'ins' => 'INSIGHTS',
    'ser' => 'SERVICES',
    'con' => 'CONTACT',
    'pri' => 'PRICING',
    'req' => 'REQUEST A FREE CONSULTATION',
  ],
];