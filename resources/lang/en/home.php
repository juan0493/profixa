<?php
return [
  'welcome1' => 'Accounting &',
  'welcome2' => 'Business Solutions',
  'p1' => '
    We provide experienced, professional and accurate accounting and business solutions tailored to your specific needs.
    <br><br>
    Our goal is provide our clients  with exceptional service, solutions that work and  be their main support 365 days a year.
    <br><br>',
  's1' => '<span>PAYROLL PROCESSING & REPORTING</span>
          Profixa365 will design and recommend a custom solution to meet your company’s specific payroll needs and be compliant with federal, state and local regulations.',
  's2' => '<span>TAX SERVICES</span>
          When it comes to tax services, Profixa365 takes a proactive approach. We are continuously identifying and researching tax laws and legislation with the sole purpose of minimizing your tax liabilities, keep you and your business in compliance and reduce the impact on  your profits.',
  's3' => '<span>BOOKKEEPING / WRITE-UP</span>
          Accurate record-keeping is fundamental to a successful business, but it can be time consuming and complicated. Profixa365 is here to help you with day-to-day bookkeeping so you can focus on what you do best, make your business grow.',
  's4' => '<span>QUICKBOOKS & SAGE 50 SET UP, MAINTENANCE & TRAINING</span>
          From beginners to advanced, Profixa365 creates a personalized training so you can become a pro-user on the different QB & Sage50 software options such as Online, Desktop and Enterprise.',
  's5' => '<span>BUSINESS CONSULTING SOLUTIONS</span>
          Whether you are a start up or established business, we have solutions for you. Our trained and hands-on experienced  professionals will identify and offer insight to implement profitable opportunities for a wide range of industries.',
  's6' => '<span>FINANCIAL MANAGEMENT SOLUTIONS</span>
          As small business owner, you may not be able to afford the high salary costs of keeping CFO on staff all year round. At Profixa365 we understand this and are available to help you with such tasks as needed:
          <br><br>
          •	​Financial Statement preparation & Analysis <br>
          •	Cost Accounting <br>
          •	Budgeting & Forecasting <br>
          •	Accounts Reconciliation, and more.',
  'ins1' => '<p class="title-notice">
              New Oregon State Wide Transit<br>
              Tax in effect starting July 1, 2018
            </p>
            <p>
              Effective July 1, 2018, Oregon employers must begin to withhold (0.001) or $1 per $1,000 of their employees wages. This tax applies to residents and non-residents working in Oregon.
            </p>
            <p>
              For more information, visit OREGON DOR webpage
              <b><a href="https://www.oregon.gov/DOR/programs/businesses/Pages/statewide-transit-tax.aspx" target="_blank">www.oregon.gov/DOR/programs/businesses/Pages/statewide-transit-tax.aspx</a></b>
            </p>',
  'ins2' => '<p class="title-notice">
              Forms for New employees
            </p>
            <p>
              It is important to keep your employee files up to date as Federal or State agencies can audit your records at any time. <a href="#!">Download them here.</a>
            </p>
            <p>
              <span>• Form W-4 for Federal Income Tax Withholding.</span>
              <span>• Form I-9 and E-Verify System for.</span>
              <span>• Employment Eligibility.</span>
              <span>• Job Application Form.</span>
            </p>',
  'ins3' => '<p class="title-notice">
              Why Goal Setting & Planning?
            </p>
            <p>
              Regardless of size and industry, goal setting and planning is the most important part of running a business.
            </p>
            <p>
              Most of startups or small business have limited resources. Therefore setting goals and planning can be a useful tool in allocating such resources where they are most needed. Furthermore, it will improve risk areas. But first things first, evaluate your business and identify those areas that need to be improved, then set your goal.  Be sure the goal is specific, measurable, attainable, relevant and of course, time bound.  When the goal is set, make an action plan, stick to it, monitor progress & celebrate your sucess!.
            </p>',
  'tes1' => 'After working with several different accountants who were not really helping me, but sending me huge bills, I was referred to Elizabeth.<br>
            She took action and in no time, she organized my accounts, suggested to make small changes to run my business more efficiently and in return, my profit grew by 50% compared to 2016. The check I write Profixa365 every month is definitely an investment.',
  'tes2' => 'There are two things that Elizabeth put into our business:  Dedication & hard work. We got peace of mind and free time for family affairs.',
  'tes3' => 'We\'ve had a great experience working with Elizabeth. Our payroll system was a mess until she came along. I highly recommend Profixa365 to anyone looking for someone reliable to take care of  business for you. By the way, she  is there on Sundays!',
  'form' => [
    't1' => 'Send us a message!',
    't2' => 'YOUR NAME*',
    't3' => 'YOUR EMAIL*',
    't4' => 'SUBJECT',
    't5' => 'YOUR MESSAGE*',
    'b' => 'SEND MESSAGE',
  ],
  'testimonial' => 'Testimonials',
];