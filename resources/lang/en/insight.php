<?php

return [
  'ins1' => '<p class="titulo">New Oregon State Wide Transit Tax in effect starting July 1, 2018</p>
            <p>
              Effective July 1, 2018, Oregon employers must begin withholding 1/10% (0.001) 
              or $1 per $1,000 of their employees\' wages. This tax applies to residents and 
              non-residents working in Oregon.
              <br><br>
              For more information, visit the Oregon Department of Revenue website or click on read more.
            </p>',
  'ins2' => '<p class="titulo">Forms for New employees</p>
            <p>
              It is important to keep your employee files up to date as Federal or State 
              agencies can audit your records. Download hiring forms here:
            </p>
            <table>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">OREGON</a></td>
                <td><a target="_blank" href="'.url("/download/eligibility").'">1-9</a></td>
                <td><a target="_blank" href="'.url("/download/federal").'">w-4 (2018)</a></td>
              </tr>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">EMPLOYMENT</a></td>
                <td><a target="_blank" href="'.url("/download/eligibility").'">EMPLOYMENT</a></td>
              </tr>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">APPLICATION</a></td>
                <td><a target="_blank" href="'.url("/download/eligibility").'">ELIGIBILITY</a></td>
              </tr>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">SAMPLE</a></td>
              </tr>
            </table>',
  'ins3' => '<p class="titulo">Why Goal Setting & Planning?</p>
            <p>
              Regardless of size and industry, goal setting and planning is the 
              most important part of running a business.
            </p>
            <p>
              Most of startups or small business have limited resources. Therefore 
              setting goals and planning can be a useful tool in allocating such 
              resources where they are most needed. Furthermore, it will improve 
              risk areas. But first things first, evaluate your business and identify 
              those areas that need to be improved, then set your goal.  Be sure the 
              goal is specific, measurable, attainable, relevant and of course, time 
              bound.  When the goal is set, make an action plan, stick to it, monitor 
              progress & celebrate your success!.
            </p>',
];
