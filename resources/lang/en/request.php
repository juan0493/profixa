<?php

return [
  'title' => 'Intake Form',
  'desc' => 'We want to get to know you, please complete the form below.',
  'name' => '*YOUR FULL NAME',
  'email' => '*EMAIL',
  'phone' => '*PHONE NUMBER',
  'cname' => 'YOUR COMPANY NAME',
  'csize' => '*YOUR COMPANY SIZE',
  'add' => 'ADDITIONAL INFORMATION (PLEASE INCLUDE BUSINESS OPERATIONS)',
  'submit' => 'Submit',
];
