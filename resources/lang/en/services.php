<?php
return [
  's1T' => 'PAYROLL PROCESSING & REPORTING',
  's1' => 'Whether you are looking to outsource payroll for the first time, want to switch from another provider or simply need assistance to process it yourself, we are there 24/7, 365 days a year.
          Our payroll solutions include:
          <br><br>
          ​• Process paychecks weekly, bi-weekly or monthly depending on 
            your schedule, all 50 states &  24/7 access to your online portal <br>
          • Handling and payment of Garnishments <br>
          • Liability calculations <br>
          • Payroll withholding filings & deposits to IRS, State, Unemployment 
            agencies as well as to your Workers compensation insurance <br>
          • W-2, 1099’s processing and distribution <br>
          • W-3,  State and Federal reconciliations at the end of the year',
  's2T' => 'TAX SERVICES',
  's2' => 'When it comes to tax services, Profixa365 takes a proactive approach. We are continuously identifying and researching tax laws and legislation with the sole purpose of minimizing your tax liabilities, keep you and your business in compliance and reduce the impact on  your profits.
          <br><br>
          Whether you are individual or business, our experience professionals will assist you with:
          <br><br>
          • Tax planing <br>
          • Tax preparation <br>
          • Compliance',
  's3T' => 'BOOKKEEPING / WRITE-UP',
  's3' => 'Accurate record-keeping is fundamental to a successful business, but it 
          can be time consuming and complicated. Accounting programs like Quickbooks®, 
          Quicken®, Peachtree®, Sage®, etc. help you record the entries into the database.
          However, choosing the wrong account or data categorization will produce 
          inaccurate records, wrong or unnecessary tax liabilities and possible penalties.
          <br><br>
          Profixa365 is here to help you with day-to-day bookkeeping so you can focus 
          on what you do best, make your business grow. We are experts in the fields 
          of business and accounting and we have compliant solutions to keep you up to 
          date, in compliance with your payroll taxes and free from penalties',
  's4T' => 'QUICKBOOKS & SAGE 50 SET UP, MAINTENANCE & TRAINING',
  's4' => 'Although QuickBooks and Sage 50 are user friendly accounting software, in 
          order to make them work for you,  obtain accurate records, tax calculations 
          and reduce the amount of time you spend doing data entry, the set up needs to 
          be correctly done and proper maintenance performed on a regular basis.
          <br><br>
          From beginners to advanced, Profixa365 offers personalized training so you 
          can learn all the tricks of these software or become a pro-user on the different 
          software options such as Online, Desktop and Enterprise. 
          <br>
          We offer individual and groups training.',
  's5T' => 'BUSINESS CONSULTING SOLUTIONS',
  's5' => 'You want to start a business and don’t know what to do first?
          <br><br>
          Your business is already in place,  but don’t know how to run the financials 
          to see if you are making a profit?  No problem, we are here to help you. Whether 
          you are a start up or established business, we have solutions for you. Our 
          trained and hands-on experienced  professionals will identify and offer insight 
          to implement profitable opportunities for a wide range of industries.
          <br><br>
          Our services include:
          <br><br>
          • Assistance in getting all necessary occupational licenses, registrations,
            tax accounts and obtaining proper insurance and workers compensation.<br>
          • Preparation of Articles of Incorporation (Corporations & LLC)<br>
          • Analysis of your  business accounting records to obtain information about 
            current revenues, expenses and tax compliance.<br>
          • Analysis of payroll payments to make sure all tax matters are taken care of.',
  's6T' => 'FINANCIAL MANAGEMENT SOLUTIONS',
  's6' => 'Your company financial condition can be a major concern for both, your business 
          and possible creditors and investors. Having a financial statement in place 
          can be a huge advantage to proactively address issues that can make a difference 
          on performance and business health.
          <br><br>
          Profixa365 can help you with:
          <br><br>
          ​• Budgeting & Forecasting<br>
          • Balance Sheet, Income statement, Cash flow statement & Trial balance<br>
          • We also offer customized monthly, quarterly and annual report packages.',
];