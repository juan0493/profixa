<?php

return [
  'titleText' => 'Nuestra Empresa',
  'text' => 'Profixa365 LLC es una empresa profesional que nace con la necesidad de transformar los servicios de contabilidad,
            finanzas y manejo de negocios convencionales y ordinarios, en soluciones personalizadas, que funcionan y con fines
            de incrementar la rentabilidad de su empresa. Nuestros profesionales son experimentados y proactivos. Ofrecen
            soluciones con fines de crecimiento a pequeñas, medianas y grandes empresas, así como a individuos.
            <br><br>
            Profixa365 se enorgullece en ofrecerle un entorno sin burocracia y teniendo siempre contacto directo con sus
            gerentes y ejecutivos. Nuestros clientes nos buscan, porque estamos aquí cuando nos necesitan, sin esperas y sin excusas.
            <br><br>
            Ofrecemos atención personalizada los 365 días del año. Nuestros clientes tienen la satisfacción de saber que
            cuando les surge algo inesperado, cuentan con nosotros. Proporcionamos conocimientos únicos, experiencia de primera mano,
            proactividad y compromiso a cada uno de nuestros clientes.',
  'missionTitle' => 'NUESTRA MISIÓN',
  'mission' => 'Nuestra misión es mejorar la rentabilidad de nuestros clientes manteniendo una comunicación abierta para brindar soluciones proactivas justo cuando es necesario. Estar ahí para atender las urgencias del cliente en cualquier momento.',
  'visionTitle' => 'NUESRA VISIÓN',
  'vision' => 'Brindar soluciones en materia de contabilidad, finanzas y manejo de negocios. Soluciones únicas, que funcionan, sin burocracia, con el mayor profesionalismo y diseñadas de acuerdo a las necesidades de cada cliente.',
  'me' => 'Como mujer de negocios, me apasiona ayudar a mis clientes a implementar sistemas que les brinden un camino para 
          dirigir sus negocios con el mínimo esfuerzo y obtener rendimientos bien merecidos. Como parte importante de mi familia 
          y comunidad, soy responsable de enseñarle a mi hija que el aprendizaje es un esfuerzo de por vida y que ese aprendizaje 
          debe aplicarse para el bienestar de ambos, nuestra familia y nuestra comunidad.',
  'bio1' => 'Elizabeth cuenta con amplia experiencia cumulativa en las áreas de administración, contabilidad, capacitación 
            en desarrollo de equipos y entrenamiento. A lo largo de su carrera, y comenzando a temprana edad mientras ayudaba 
            en el negocio familiar, aprendió la importancia crucial de la gestión financiera. Elizabeth ha trabajado para 
            organizaciones grandes y pequeñas apoyando sus esfuerzos de contabilidad y recursos humanos en varios formatos. 
            Tiene una larga trayectoria en el área de Salem, trabajo para Monrovia durante casi 10 años en operaciones de planificación 
            y presupuestarias, así como capacitación y desarrollo de personal en prácticas de sistemas informáticos internos. Además, 
            ha acumulado una amplia experiencia en la industria de la construcción, con una comprensión profunda del ciclo comercial 
            de la industria desde la contratación, gestión de proyectos, compras de materiales, facturación y previsión, gestión laboral, 
            así como la gestión de las funciones de recursos humanos.',
  'bio2' => 'Elizabeth se siente más cómoda desarrollando e implementando sistemas de enfoques estratégicos para ayudar a sus clientes a 
            alcanzar sus metas financieras. A la vanguardia de estos esfuerzos, es asegurarse de que los sistemas brinden valor agregado y 
            faciliten a la compañía obtener un alto retorno de la inversión. Ella cree que la ganancia sobre la inversión se puede medir y analizar 
            en todos los aspectos del ciclo comercial. Actualmente, Elizabeth se asegura de que los procedimientos de cuentas pagables, cobranza, 
            nómina, facturación y recursos humanos se mantengan actualizados y se implementen de forma efectiva en todos los negocios que usan sus 
            servicios. Esto también incluye las siguiente: leyes laborales salariales y horas de trabajo, requisitos de compensación laboral, cumplimiento 
            tributario, IRS y declaraciones estatales.
            <br>          
            Esta experiencia acumulada en los campos de finanzas y contabilidad le ha dado a Elizabeth la oportunidad de convertirse 
            en un buen miembro de equipo flexible capaz de cerrar la brecha en muchas situaciones diferentes en el entorno comercial 
            y empresarial. Elizabeth, está totalmente comprometida con el éxito de sus clientes al proporcionar un conjunto proactivo 
            único de soluciones para su negocio.',
];
