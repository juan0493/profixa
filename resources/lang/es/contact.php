<?php

return [
  'title' => 'Nuestros Costos',
  't1' => 'Cada uno de nuestros clientes es único y nuestras soluciones son estratégicamente diseñadas para satisfacer las necesidades de su empresa y contribuir a incrementar sus ganancias.',
  't2' => 'Nuestros costos es algo de lo que no se tiene que preocupar ya que típicamente son 50% más bajos que nuestros competidores.',
  'link' => 'SOLICITE PRESUPUESTO',
];