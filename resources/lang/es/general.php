<?php
return [
  'about' => 'Nosotros',
  'services' => 'Servicios',
  'pricing' => 'Costos',
  'contact' => 'Contacto',
  'rmore' => 'leer más',
  'insight' => 'Interés',
  'menu' => [
    'hom' => 'INICIO',
    'abo' => 'NOSOTROS',
    'ins' => 'INSIGHTS',
    'ser' => 'SERVICIOS',
    'con' => 'CONTACTO',
    'pri' => 'COSTOS',
    'req' => 'CONSULTA GRATIS',
  ],
];
