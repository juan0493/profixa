<?php
return [
  'welcome1' => 'Soluciones en contabilidad',
  'welcome2' => 'Gestion & Administracion empresarial',
  'p1' => '
    Brindamos soluciones especializadas en materia de contabilidad, gestión y administración empresarial de manera responsable y adaptadas a las necesidades de usted y su negocio
    <br><br>
    Soluciones proactivas, con fines de crecimiento & brindandole apoyo los 365 dias del año<br>',
  's1' => '<span>Control & declaracion de nomina</span>
          Profixa365 diseñará y recomendará una solución personalizada para  el proceso, control y declaracion de la nómina de su empresa. Solución que cumpla con las regulaciones federales, estatales y locales.',
  's2' => '<span>Servicio de impuestos</span>
          Cuando se trata de impuestos, Profixa365 toma un enfoque proactivo. Estamos continuamente identificando y haciendo investigaciones sobre leyes y regulaciones fiscales con  el único propósito de minimizar sus obligaciones tributarias, mantenerlo a usted y a su negocio en  cumplimiento y reducir el impacto que esto tiene en sus  ganancias.',
  's3' => '<span>Gestion contable</span>
          La gestión contable es fundamental para llevar su negocio al exito, pero esto se puede llevar mucho tiempo y ser complicado. Para que usted pueda concentrarse en mejorar y crecer su negocio, Profixa365 está aquí para ayudarlo con la contabilidad diaria.',
  's4' => '<span>Instalacion, mantenimiento & capacitacion de software contable: quickbooks & sage 50</span>
          Desde principiantes hasta avanzados, Profixa365 creará e implementará un plan de capacitación personalizado para que usted pueda usar cualquiera de las diferentes opciones en  software:  QuickBooks & Sage 50 Online, Desktop and Enterprise.',
  's5' => '<span>Asesoria  en manejo de negocios</span>
          Cualquiera que sea la industria, el tamaño, si usted es tiene una empresa nueva o una ya establecida, contamos con soluciones para crecer y mejorar sus ganancias. Nuestros profesionales son experimentados en negocios y le ofrecerán información de valor para identificar oportunidades de crecimiento y rentabilidad.',
  's6' => '<span>Analisis y gestion financiera</span>
          Como dueño de una pequeña empresa, es posible que usted no pueda pagar el alto costo de mantener a un Gestor Financiero de tiempo completo en su empresa. En Profixa365 lo entendemos y estamos disponibles para ayudarlo con las siguientes funciones de manera inmediata:
          <br><br>
          •	Preparación y análisis de estados financieros. <br>
          •	Pronósticos & presupuestos. <br>
          •	Control presupuestario. <br>
          •	Conciliaciónes bancarias y mucho más.',
  'ins1' => '<p class="title-notice">
              En Julio 1ro del 2018<br>Entrara en efecto el Nuevo Impuesto Estatal
            </p>
            <p>
              A partir del 1ro de Julio del 2018, todos los empleadores de Oregon empesaran a deducir 1/10% (.001) o $1 por cada $1000 del salario de sus empleados. Este impuesto aplica a residentes y no residentes que trabajan en el estado de Oregon.
            </p>
            <p>
              Para obtener mas información, viste el sitio de Oregon Department of Revenue
              <b><a href="https://www.oregon.gov/DOR/programs/businesses/Pages/statewide-transit-tax.aspx" target="_blank">
              www.oregon.gov/DOR/programs/businesses/Pages/statewide-transit-tax.aspx
              </a></b>.
            </p>',
  'ins2' => '<p class="title-notice">
              Descargue Formularios Para Contratacion de Empleados
            </p>
            <p>
              Es importante mantener al dia los archivos de personal, ya que las agencias Estatales y Federales pueden conducir auditorias en cualquier momento.
            </p>
            <p>
              <span>• Solicitud de Empleo.</span>
              <span>•	i-9 Elegibilidad.</span>
              <span>•	W4- Federal</span>
            </p>',
  'ins3' => '<p class="title-notice">
              Porque es importante fijar metas y planificar?
            </p>
            <p>
              Independientemente del tamaño y la industria, establecer metas y la planificación son la parte más importante de la gestión de una empresa.
            </p>
            <p>
              La mayoría de las nuevas empresas o pequeñas empresas tienen recursos limitados. Por lo tanto, establecer metas y planificar puede ser una herramienta útil para asignar dichos recursos donde son más necesarios. Además, mejorará las zonas de riesgo. Pero lo primero es lo primero, evalúe su negocio e identifique las áreas que deben mejorarse, luego establezca sus metas. Asegúrese de que el objetivo sea específico, medible, alcanzable, relevante y por supuesto, durante el tiempo establecido. Cuando se establezca la meta, haga un plan de acción, cúmplalo, supervise el progreso y celebre su éxito.
            </p>',
  'tes1' => 'Después de trabajar con varios contadores diferentes que realmente no me estaban ayudando, pero que me enviaban facturas enormes, me remitieron a Elizabeth.<br>
            Tomó medidas y en ningún momento, organizó mis cuentas, sugirió hacer pequeños cambios para administrar mi negocio de manera más eficiente y, a cambio, mi beneficio creció un 50% en comparación con 2016. El cheque que escribo en Profixa365 todos los meses es definitivamente una inversión.',
  'tes2' => 'Hay dos cosas que Elizabeth puso en nuestro negocio: Dedicación y trabajo duro. Conseguimos tranquilidad y tiempo libre para asuntos familiares.',
  'tes3' => 'Hemos tenido una gran experiencia trabajando con Elizabeth. Nuestro sistema de nómina era un desastre hasta que llegó. Recomiendo Profixa365 a cualquiera que busque a alguien confiable para que se encargue de los negocios por usted. Por cierto, ella está allí los domingos!',
  'form' => [
    't1' => '¡Envíanos un mensaje!',
    't2' => 'TU NOMBRE*',
    't3' => 'TU EMAIL*',
    't4' => 'ASUNTO',
    't5' => 'TU MENSAJE*',
    'b' => 'ENVIAR MENSAJE',
  ],
  'testimonial' => 'Testimonios',
];
