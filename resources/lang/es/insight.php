<?php

return [
  'ins1' => '<p class="titulo">En Julio 1ro del 2018, Entrara en efecto el Nuevo Impuesto Estatal</p>
            <p>
              A partir del 1ro de Julio del 2018, todos los empleadores de Oregon empesaran a deducir 1/10% (.001) o $1 por cada $1000 del salario de sus empleados. Este impuesto aplica a residentes y no residentes que trabajan en el estado de Oregon.
              <br><br>
              Para obtener mas información, viste el sitio de Oregon Department of Revenue
              <b><a href="https://www.oregon.gov/DOR/programs/businesses/Pages/statewide-transit-tax.aspx" target="_blank">
              www.oregon.gov/DOR/programs/businesses/Pages/statewide-transit-tax.aspx
              </a></b>.
            </p>',
  'ins2' => '<p class="titulo">Descargue Formularios Para Contratacion de Empleados</p>
            <p>
              Es importante mantener al dia los archivos de personal, ya que las agencias Estatales y Federales pueden conducir auditorias en cualquier momento.
            </p>
            <table>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">OREGON</a></td>
                <td><a target="_blank" href="'.url("/download/eligibility").'">1-9</a></td>
                <td><a target="_blank" href="'.url("/download/federal").'">w-4 (2018)</td>
              </tr>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">EMPLEO</a></td>
                <td><a target="_blank" href="'.url("/download/eligibility").'">EMPLEO</a></td>
              </tr>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">SOLICITUD</a></td>
                <td><a target="_blank" href="'.url("/download/eligibility").'">ELEGIBILIDAD</a></td>
              </tr>
              <tr>
                <td><a href="'.url("/download/application").'" target="_blank">MUESTRA</a></td>
              </tr>
            </table>',
  'ins3' => '<p class="titulo">Porque es importante fijar metas y planificar?</p>
            <p>Independientemente del tamaño y la industria, establecer metas y la planificación son la parte más importante de la gestión de una empresa.</p>
            <p>
              La mayoría de las nuevas empresas o pequeñas empresas tienen recursos limitados. Por lo 
              tanto, establecer metas y planificar puede ser una herramienta útil para asignar dichos 
              recursos donde son más necesarios. Además, mejorará las zonas de riesgo. Pero lo primero 
              es lo primero, evalúe su negocio e identifique las áreas que deben mejorarse, luego 
              establezca sus metas. Asegúrese de que el objetivo sea específico, medible, alcanzable, 
              relevante y por supuesto, durante el tiempo establecido. Cuando se establezca la meta, 
              haga un plan de acción, cúmplalo, supervise el progreso y celebre su éxito.
            </p>',
];
