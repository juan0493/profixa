<?php

return [
  'title' => 'Formulario de consulta',
  'desc' => 'Permítanos saber un poco más de usted, por favor complete el formulario.',
  'name' => '*NOMBRE COMPLETO',
  'email' => '*EMAIL',
  'phone' => '*TELÉFONO',
  'cname' => 'NOMBRE DE SU EMPRESA',
  'csize' => '*TAMAÑO DE SU EMPRESA',
  'add' => 'MENSAJE (POR FAVOR INCLUYA SU INDUSTRIA)',
  'submit' => 'Enviar',
];
