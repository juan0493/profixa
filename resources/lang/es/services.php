<?php
return [
  's1T' => 'PROCESO, CONTROL & DECLARACION DE NOMINA',
  's1' => 'Ya sea que usted busque externalizar la nómina de su negocio por primera vez, deseé cambiar su actual proveedor o simplemente necesita ayuda para procesar la nómina usted mismo, Profixa365 esta disponibles las 24 horas del día, los 7 días de la semana, los 365 días del año
          Nuestras soluciones de nómina incluyen: <br><br>
          •	Creacion de cheques de pago:  semanal, quincenal o mensual, dependiendo de su itinerario. Sirviendole en cualquiera de los 50 estados dentro de los Estados Unidos y contamos con acceso a su portal en linea las 24 horas del dia, los 7 dias de las semana.<br>
          •	Manejo y pago de embargos a nómina.<br>
          •	Cálculo de obligaciones fiscales.<br>
          •	Cálculo y declaración de retención de impuestos al IRS, Estado y Agencia de Desempleo, asi como al Seguro de Compensación de Trabajadores.<br>
          •	Proceso y distribución  de W-2, 1099’s  & W3.<br>
          •	Conciliación anual de Estado & Federal.',
  's2T' => 'SERVICIO DE IMPUESTOS',
  's2' => 'Cuando se trata de impuestos, Profixa365 toma un enfoque proactivo. Estamos continuamente identificando y haciendo investigaciones sobre leyes y regulaciones fiscales con  el único propósito de minimizar sus obligaciones tributarias, mantenerlo a usted y a su negocio en  cumplimiento y reducir el impacto que esto tiene en sus  ganancias.<br><br>
          Independientemente que usted sea un particular o una empresa, nuestro experimentado equipo profesional le ayudara con:<br><br>
          •	Planificación de impuestos<br>
          •	Preparación de impuestos<br>
          •	Cumplimiento tributario',
  's3T' => 'GESTION CONTABLE',
  's3' => 'La contabilidad es un medio que asegura el crecimiento de su negocio, por lo que la información contable es un instrumento fundamental e indispensable para la toma de decisiones en materia economica, laboral y tributaria.<br><br>
          Software contable como Quickbooks, Sage 50, Quicken, etc. le ayuda a registrar las transacciones diarias. Sin embargo, categorizar y elegir las cuentas incorrectas producirán resultados erróneos, obligaciones fiscales innecesarias y posibles penalidades.<br>
          Profixa365 esta aquí para ayudarlo día a día para que su negocio funcione correctamente y en regla con lo que dispone la ley en este campo. <br><br>
          Nuestras soluciones en contabilidad busca darle a usted dueño de negocio o emprendedor, el aseramiento, planificación y control adecuado para que usted pueda analizar, determinar la rentabilidad de su negocio y ser proactivo en la toma de decisiones.',
  's4T' => 'INSTALACION, MANTENIMIENTO & CAPACITACION DE SOFTWARE CONTABLE: QUICKBOOKS & SAGE 50',
  's4' => 'Aunque QuickBooks y Sage 50 son software de contabilidad fácil de usar, para que estos funcionen, obtenga registros precisos, cálculo de obligaciones fiscales correctos y reduzca la cantidad de tiempo que dedica a ingresar datos, es necesario que la configuración se realice correctamente y se le de mantenimiento de manera regular.  <br><br>
          Desde principiantes hasta avanzados, Profixa365 le ofrece capacitación personalizada para que usted pueda aprender todos los trucos de estos softwares o convertirse en un usuario profesional en Sage50 & Quickbooks Online, Desktop and Enterprise. Ofrecemos capacitación individual y grupos.',
  's5T' => 'ASESORIA EN MANEJO DE NEGOCIOS',
  's5' => '¿Quieres establecer un negocio y no sabe por dónde empezar?<br>
          ¿Su negocio ya está en marcha, pero no sabe cómo analizar las finanzas para ver si está obteniendo ganancias? No hay problema, estamos aquí para ayudarlo. <br>
          Cualquiera que sea la industria, el tamaño, si usted es tiene una empresa nueva o una ya establecida, contamos con soluciones para crecer y aumentar sus ganancias. Nuestros profesionales son experimentados en negocios y le ofrecerán información de valor para identificar oportunidades de crecimiento y rentabilidad.<br>
          Nuestras soluciones incluyen:<br><br>
          •	 Asistencia para obtener todas las licencias ocupacionales, registros, darse de alta con el IRS & el Estado, así como y obtener un seguro para la compensación de los trabajadores<br>
          •	Preparación de  artículos de Incorporación (Corporations & LLC).<br>
          •	  Análisis contable y nómina de su empresa para obtener información sobre ingresos, gastos y asegurarse que están en cumplimiento fiscal y tributario.',
  's6T' => 'ANALISIS & GESTION FINANCIERA',
  's6' => 'Profixa365 le ofrece soluciones en: <br><br>
          •	Preparación y análisis de estados financieros.<br>
          •	Pronósticos & presupuestos.<br>
          •	Control presupuestario<br>
          •	Conciliaciones bancarias y mucho más.',
];