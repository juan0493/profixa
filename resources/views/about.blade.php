@extends('layouts.app')

@section('title', 'About us')

@section('content')
<header class="home content-bottom">
  <div class="content-profixa">
    @include('shared.menu')
    <h1 class="main-title">
      @lang('general.about')
    </h1>
  </div>
</header>

<section class="moveUp square-back">
  <div class="top">
    <p class="simple-text gray-text">
      <span>@lang('about.titleText')</span>
      @lang('about.text')
    </p>
    <img src="{{ asset('img/About1.jpg') }}" alt="Our Firm">
  </div>
  <div class="bottom">
    <div class="photo hide-img">
      <img src="{{ asset('img/About2.jpg') }}" alt="Elizabeth">
      <p>Elizabeth G. Martinez, President</p>
    </div>
    <div class="texts">
      <p class="simple-text blue-text-about">
        <span>@lang('about.missionTitle')</span>
        @lang('about.mission')
      </p>
      <p class="simple-text lightblue-text">
        <span>@lang('about.visionTitle')</span>
        @lang('about.vision')
      </p>
      <div class="photo hide">
        <img src="{{ asset('img/About2.jpg') }}" alt="Elizabeth">
        <p>Elizabeth G. Martinez, President</p>
      </div>
      <p class="comma-back">@lang('about.me')</p>
    </div>
  </div>
</section>

<section class="celeste section">
  <div class="row content-profixa-text">
    <div class="col s12 m6 l6 xl6">
      @lang('about.bio1')
    </div>
    <div class="col s12 m6 l6 xl6">
      @lang('about.bio2')
    </div>
  </div>
</section>

@include('shared.footer')
@endsection
