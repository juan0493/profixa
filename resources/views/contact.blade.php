@extends('layouts.app')

@section('title', 'Contact')

@section('content')
<header class="home content-bottom-contact">
  <div class="content-profixa">
    @include('shared.menu')
  </div>
</header>

<section id="contact">
  <h2 class="title-gray">@lang('general.contact')</h2>
  <a
    target="_blank"
    href="https://www.google.com/maps/search/?api=1&amp;query=44.990122,-123.026208"
    class="map"
    title="Google Maps"></a>
  <form id="sendMail">
    <div class="content-profixa">
      <span>@lang('home.form.t1')</span>
      <div class="row">
        <div class="col s12 m6 l6 xl6">
          <div class="input-field col s12">
            <input id="home-name" name="home-name" required type="text" class="validate">
            <label for="home-name">@lang('home.form.t2')</label>
          </div>
          <div class="input-field col s12">
            <input id="home-email" name="home-email" required type="email" class="validate">
            <label for="home-email">@lang('home.form.t3')</label>
          </div>
          <div class="input-field col s12">
            <input id="home-subject" name="home-subject" required type="text" class="validate">
            <label for="home-subject">@lang('home.form.t4')</label>
          </div>
        </div>
        <div class="col s12 m6 l6 xl6">
          <div class="input-field col s12">
            <textarea name="home-msg" id="home-msg" cols="30" rows="10" required></textarea>
            <label for="home-msg">@lang('home.form.t5')</label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <button class="button-blue" type="submit">@lang('home.form.b')</button>
        </div>
      </div>
    </div>
  </form>
</section>

@include('shared.footer')
@endsection
