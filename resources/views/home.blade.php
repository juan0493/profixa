@extends('layouts.app')

@section('title', 'Home')

@section('content')
<header class="home">
  <div class="content-profixa">
    @include('shared.menu')
    <h1 class="main-title">
      <span>@lang('home.welcome1')</span>
      @lang('home.welcome2')
    </h1>
    <a href="{{ url('about-us') }}"><button class="button-blue">@lang('general.about')</button></a>
    <div class="top-content">
      <div>
        <p>@lang('home.p1')</p>
        <a href="{{ url('/services') }}"><button class="button-green">@lang('general.rmore')</button></a>
      </div>
      <img src="{{asset('img/image1.jpg')}}" alt="Read more">
    </div>
  </div>
</header>

<section class="content-profixa margin-top" id="news">
  <h2>@lang('general.services')</h2>

  <div class="row">
    <div class="col s12 m12 l6 xl6">
      <div class="grid-content">
        <img src="{{asset('img/content1.jpg')}}" alt="Profixa">
        <div>
          <p>@lang('home.s1')</p>
          <a href="{{ url('/services') }}#PAYROLL"><button class="button-green">@lang('general.rmore')</button></a>
        </div>
      </div>
    </div>
    <div class="col s12 m12 l6 xl6">
      <div class="grid-content">
        <img src="{{asset('img/content3.jpg')}}" alt="Profixa">
        <div>
          <p>@lang('home.s2')</p>
          <a href="{{ url('/services') }}#TAX-SERVICES"><button class="button-green">@lang('general.rmore')</button></a>
        </div>
      </div>
    </div>
    <div class="col s12 m12 l6 xl6">
      <div class="grid-content">
        <img src="{{asset('img/content2.jpg')}}" alt="Profixa">
        <div>
          <p>@lang('home.s3')</p>
          <a href="{{ url('/services') }}#BOOKKEEPING"><button class="button-green">@lang('general.rmore')</button></a>
        </div>
      </div>
    </div>
    <div class="col s12 m12 l6 xl6">
      <div class="grid-content">
        <img src="{{asset('img/content.jpg')}}" alt="Profixa">
        <div>
          <p>@lang('home.s4')</p>
          <a href="{{ url('/services') }}#QUICKBOOKS"><button class="button-green">@lang('general.rmore')</button></a>
        </div>
      </div>
    </div>
    <div class="col s12 m12 l6 xl6">
      <div class="grid-content">
        <img src="{{asset('img/content-large1.jpg')}}" alt="Profixa">
        <div>
          <p>@lang('home.s5')</p>
          <a href="{{ url('/services') }}#BUSINESS"><button class="button-green">@lang('general.rmore')</button></a>
        </div>
      </div>
    </div>
    <div class="col s12 m12 l6 xl6">
      <div class="grid-content">
        <img src="{{asset('img/content-large.jpg')}}" alt="Profixa">
        <div>
          <p>@lang('home.s6')</p>
          <a href="{{ url('/services') }}#FINANCIAL "><button class="button-green">@lang('general.rmore')</button></a>
        </div>
      </div>
    </div>
  </div>

  <div class="aux-background"></div>
</section>

<section id="insights">
  <div class="content-profixa">
    <h2>@lang('general.insight')</h2>

    <div class="grid">
      <div class="column l">
        <div class="notice">
          <img src="{{asset('img/notice1.jpg')}}" alt="Notice">
          <time datetime="2018-06-14 20:00">June 14, 2018</time>
          @lang('home.ins1')
          <p class="name center-align">
            Elizabeth
          </p>
          <div class="green-mail center-align">elizabeth@profixa365.com</div>
        </div>
      </div>
      <div class="column r">
        <div class="notice image">
          <div class="aux-bg"></div>
          <div class="img">
            <img src="{{asset('img/notice2.jpg')}}" alt="Notice">
            <div class="green-mail center-align">elizabeth@profixa365.com</div>
          </div>
          
          <div class="content-notice">
            <time datetime="2018-06-15 20:00">June 15, 2018</time>
            @lang('home.ins2')
            <p class="name">
              Elizabeth
            </p>
          </div>

          <div class="hide green-mail center-align">elizabeth@profixa365.com</div>
        </div>
        <div class="notice">
          <time datetime="2018-06-15 20:00">June 15, 2018</time>
          @lang('home.ins3')
          <p class="name">
            Elizabeth
          </p>
          <div class="green-mail center-align max">elizabeth@profixa365.com</div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="testimonials" class="content-profixa comma-back">
  <h2>@lang('home.testimonial')</h2>
  <div class="people">
    <p class="blue-people">
      <span>
        Cabrebra<br>Contracting
      </span>
      @lang('home.tes1')
    </p>

    <p class="lightblue-people">
      <span>
        Lucas J.<br>Bell
      </span>
      @lang('home.tes2')
    </p>

    <p class="green-people">
      <span>
        Laura<br>Ascencio
      </span>
      @lang('home.tes3')
    </p>
  </div>
</section>

@include('shared.footer')
@endsection
