@extends('layouts.app')

@section('title', 'Insights')

@section('content')
<header class="home">
  <div class="content-profixa">
    @include('shared.menu')
    <h1 class="main-title">
      Insights
    </h1>
  </div>
</header>

<section class="insights-content section">
  <div class="gray-back">
    <img src="{{ asset('img/insights1.jpg') }}" alt="Insights">
    <div>
      @lang('insight.ins1')
      <a
        href="https://www.oregon.gov/DOR/programs/businesses/Pages/statewide-transit-tax.aspx"
        target="_blank"
        class="button-green">
        @lang('general.rmore')
      </a>
    </div>
  </div>
  <div class="gray-back">
    <img src="{{ asset('img/insights2.jpg') }}" alt="Insights">
    <div>
      @lang('insight.ins2')
      <a 
        href="https://www.dol.gov/oasam/orientation/forms-newemployee.htm" 
        target="_blank" 
        class="button-green">
        @lang('general.rmore')
      </a>
    </div>
  </div>
  <div class="gray-back">
    <img src="{{ asset('img/insights3.jpg') }}" alt="Insights">
    <div>
      @lang('insight.ins3')
      <a href="{{ url('/contact') }}" class="button-green">@lang('general.rmore')</a>
    </div>
  </div>
</section>

@include('shared.footer')
@endsection
