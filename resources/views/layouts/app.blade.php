<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PROFIXA365 - @yield('title')</title>
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('manifest.json')}}">
    <meta name="msapplication-TileColor" content="#41a042">
    <meta name="msapplication-TileImage" content="{{asset('ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#41a042">
  </head>
  <body>
    @yield('content')
    @if (strtoupper(session('lang')) === 'EN')
      <script>
        var emailMessages = {
          errEmail: 'INVALID DATA',
          sending: 'Sending data...',
          close: 'Close',
          errorTitle: 'Something went wrong',
          errorMessage: 'Your message could not be sent at the moment, try again later',
          successTitle: 'Sent!',
          successMessage: 'Message sent successfully',
        };
      </script>
    @else
      <script>
        var emailMessages = {
          errEmail: 'DATOS INVÁLIDOS',
          sending: 'Enviando datos...',
          close: 'Cerrar',
          errorTitle: 'Algo salió mal',
          errorMessage: 'Tu mensaje no pudo ser enviado, intenta nuevamente más tarde.',
          successTitle: 'Enviado!',
          successMessage: 'Mensaje enviado satisfactoriamente',
        };
      </script>
    @endif
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link
      rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
      integrity="sha256-OweaP/Ic6rsV+lysfyS4h+LM6sRwuO3euTYfr6M124g=" 
      crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"
      integrity="sha256-U/cHDMTIHCeMcvehBv1xQ052bPSbJtbuiw4QA9cTKz0="
      crossorigin="anonymous"></script>
    <script src="{{asset('js/app.js')}}"></script>
  </body>
</html>
