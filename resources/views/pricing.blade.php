@extends('layouts.app')

@section('title', 'Pricing')

@section('content')
<header class="home content-bottom-contact">
  <div class="content-profixa">
    @include('shared.menu')
  </div>
</header>

<section class="contact extra">
  <div>
    <h2>@lang('contact.title')</h2>
    <p>@lang('contact.t1')</p>
    <p>@lang('contact.t2')</p>
    <a class="button-green" href="{{ url('/contact') }}">@lang('contact.link')</a>
  </div>
  <img src="{{ asset('img/contact.png') }}" alt="Pricing">
</section>

@include('shared.footer')
@endsection
