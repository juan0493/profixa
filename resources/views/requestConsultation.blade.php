@extends('layouts.app')

@section('title', 'Request a free consultation')

@section('content')
<header class="home">
  <div class="content-profixa">
    @include('shared.menu')
    <h1 class="main-title">
      @lang('request.title')
    </h1>
  </div>
</header>

<section class="take-form">
  <form id="sendMailRequest">
    <div class="content-profixa">
      <span>@lang('request.desc')</span>
      <div class="row">
        <div class="col s12 m6 l6 xl6">
          <div class="input-field col s12">
            <input id="request-name" name="request-name" required type="text" class="validate">
            <label for="request-name">@lang('request.name')</label>
          </div>
          <div class="input-field col s12">
            <input id="request-email" name="request-email" required type="email" class="validate">
            <label for="request-email">@lang('request.email')</label>
          </div>
          <div class="input-field col s12">
            <input id="request-pnumber" name="request-pnumber" required type="text" class="validate">
            <label for="request-pnumber">@lang('request.phone')</label>
          </div>

          <div class="input-field col s12">
            <input id="request-company_name" name="request-company_name" type="text" class="validate">
            <label for="request-company_name">@lang('request.cname')</label>
          </div>
          <div class="input-field col s12">
            <input id="request-company_size" name="request-company_size" required type="text" class="validate">
            <label for="request-company_size">@lang('request.csize')</label>
          </div>
        </div>
        <div class="col s12 m6 l6 xl6">
          <div class="input-field col s12">
            <textarea
              placeholder="@lang('request.add')"
              name="request-msg" 
              id="request-msg"
              required></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <button class="button-green" type="submit">@lang('request.submit')</button>
        </div>
      </div>
    </div>
  </form>
</section>

@include('shared.footer')
@endsection
