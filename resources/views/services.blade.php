@extends('layouts.app')

@section('title', 'Services')

@section('content')
<header class="home content-bottom">
  <div class="content-profixa">
    @include('shared.menu')
    <h1 class="main-title">
      @lang('general.services')
    </h1>
  </div>
</header>

<section class="services content">
  <div id="PAYROLL" class="service-content">
    <img src="{{ asset('img/services1.jpg') }}" alt="Services">
    <p>
      <span>@lang('services.s1T')</span>
      @lang('services.s1')
    </p>
  </div>
  <div id="TAX-SERVICES" class="service-content">
    <img src="{{ asset('img/services2.jpg') }}" alt="Services">
    <p>
      <span>@lang('services.s2T')</span>
      @lang('services.s2')
    </p>
  </div>
  <div id="BOOKKEEPING" class="service-content">
    <img src="{{ asset('img/services3.jpg') }}" alt="Services">
    <p>
      <span>@lang('services.s3T')</span>
      @lang('services.s3')
    </p>
  </div>
  <div id="QUICKBOOKS" class="service-content">
    <img src="{{ asset('img/services4.jpg') }}" alt="Services">
    <p>
      <span>@lang('services.s4T')</span>
      @lang('services.s4')
    </p>
  </div>
  <div id="BUSINESS" class="service-content">
    <img src="{{ asset('img/services5.jpg') }}" alt="Services">
    <p>
      <span>@lang('services.s5T')</span>
      @lang('services.s5')
    </p>
  </div>
  <div id="FINANCIAL" class="service-content">
    <img src="{{ asset('img/services6.jpg') }}" alt="Services">
    <p>
      <span>@lang('services.s6T')</span>
      @lang('services.s6')
    </p>
  </div>
</section>

@include('shared.footer')
@endsection
