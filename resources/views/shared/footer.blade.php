<footer>
  <ul>
    <li>
      <a target="_blank" href="https://www.google.com/maps/search/?api=1&amp;query=44.990122,-123.026208">
        <span class="icons map"></span>
        PO BOX 20341 KEIZER, OR 97307
      </a>
    </li>
    <li>
      <a href="mailto:info@profixa365.com?Subject=Profixa365">
        <span class="icons mail"></span>
        info@profixa365.com
      </a>
    </li>
    <li>
      <a href="tel:+5034099634">
        <span class="icons tel"></span>
        (503) 409 9634
      </a>
    </li>
  </ul>
  <div class="social">
    <a href="https://www.facebook.com/Profixa365" target="_blank"><span class="social-icon fb"></span></a>
    <a href="https://www.instagram.com" target="_blank"><span class="social-icon in"></span></a>
    <a href="https://twitter.com" target="_blank"><span class="social-icon tw"></span></a>
  </div>
  <img src="{{asset('img/logo.png')}}" alt="Profixa" class="responsive-img logo-footer">
  <p>
    © Copyright Profixa365
  </p>
</footer>