<div class="menu-head">
  <a href="{{ url('/') }}"><img src="{{asset('img/logoT.png')}}" alt="Profixa365" id="logo-profixa"></a>
  <a href="#!" data-target="slide-out" class="sidenav-trigger">
    <i class="material-icons">menu</i>
  </a>
  <ul>
    <li><a href="{{url('/')}}">@lang('general.menu.hom')</a></li>
    <li><a href="{{url('/about-us')}}">@lang('general.menu.abo')</a></li>
    <li><a href="{{url('/insights')}}">@lang('general.menu.ins')</a></li>
    <li><a href="{{url('/services')}}">@lang('general.menu.ser')</a></li>
    <li><a href="{{url('/contact')}}">@lang('general.menu.con')</a></li>
    <li><a href="{{url('/pricing')}}">@lang('general.menu.pri')</a></li>
    <li><a href="{{ url('/request-a-free-consultation') }}">@lang('general.menu.req')</a></li>
    <li>
      <span>
        <a href="{{ url('/language/'.(strtoupper(session('lang')) === 'EN' ? 'es' : 'en')) }}">
          {{ strtoupper(session('lang')) === 'EN' ? 'ESPAÑOL' : 'ENGLISH' }}
        </a>
      </span>
    </li>
  </ul>
</div>
<ul id="slide-out" class="sidenav">
  <li><img src="{{asset('img/logo.png')}}" alt="Profixa365" class="responsive-img"></li>
  <li><a href="{{url('/')}}">@lang('general.menu.hom')</a></li>
  <li><a href="{{url('/about-us')}}">@lang('general.menu.abo')</a></li>
  <li><a href="{{url('/insights')}}">@lang('general.menu.ins')</a></li>
  <li><a href="{{url('/services')}}">@lang('general.menu.ser')</a></li>
  <li><a href="{{url('/contact')}}">@lang('general.menu.con')</a></li>
  <li><a href="{{url('/pricing')}}">@lang('general.menu.pri')</a></li>
  <li><a href="{{ url('/request-a-free-consultation') }}">@lang('general.menu.req')</a></li>
  <li>
    <a href="{{ url('/language/'.(strtoupper(session('lang')) === 'EN' ? 'es' : 'en')) }}">
      <span class="language-item">
        {{ strtoupper(session('lang')) === 'EN' ? 'ESPAÑOL' : 'ENGLISH' }}
      </span>
    </a>
  </li>
</ul>
      