<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['SetLang'])->group(function () {
  Route::get('/', 'HomeController@index');
  Route::get('/about-us', 'HomeController@aboutUs');
  Route::get('/insights', 'HomeController@insights');
  Route::get('/services', 'HomeController@services');
  Route::get('/contact', 'HomeController@contact');
  Route::get('/pricing', 'HomeController@pricing');
  Route::get('/request-a-free-consultation', 'HomeController@requestConsultation');    

  Route::get('/language/{lan}', 'HomeController@language');

  Route::get('/download/{id}', 'HomeController@download');
});

Route::post('/contact-message', 'SendMailController@home');
Route::post('/request-message', 'SendMailController@request');

